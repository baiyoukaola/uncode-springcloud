package cn.uncode.springcloud.admin.controller.eureka;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.uncode.springcloud.admin.model.eureka.EurekaInstanceBo;
import cn.uncode.springcloud.admin.service.eureka.EurekaService;
import cn.uncode.springcloud.starter.web.result.R;

@RestController
@RequestMapping("/eureka")
public class EurekaController {
	
	@Autowired
	private EurekaService eurekaService;
	
	@RequestMapping("/apps")
    public R<List<EurekaInstanceBo>> loadDeskIcons() {
		List<EurekaInstanceBo> list = eurekaService.getAllApps();
    	return R.success(list);
    }
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public R<Boolean> update(@RequestParam String name, @RequestParam String instanceId, @RequestParam String status){
		eurekaService.updateStatus(name, instanceId, status);
		return R.success();
	}
		      
	
//	@RequestMapping(value = "status", method = RequestMethod.GET)
//	public ResultMap getStatus(){
//		return ResultMap.buildSuccess().put("status", serviceRegistry.getStatus(registration));
//	}
//	
//	@RequestMapping(value = "app/{appName}/{instanceId}/{status}", method = RequestMethod.GET)
//	public ResultMap setStatus(@PathVariable String appName, @PathVariable String instanceId, @PathVariable String status){
//		//http://localhost:1001/eureka/apps/UNCODE-ADMIN/DESKTOP-JL9T2IK:uncode-admin:8000/status?value=DOWN
//		Map<String, String> headers = new HashMap<>();
//		headers.put("Content-Type", "text/plain");
//		String path = AppInfo.getEnv().getProperty("eureka-server-url") + "/apps/" + appName + "/" + instanceId + "/status";
//		Map<String, String> params = new HashMap<>();
//		params.put("value", status);
////		HttpUtil.put(path, params, headers);
//		return ResultMap.buildSuccess();
//	}
//	
//	@RequestMapping(value = "status/{appName}", method = RequestMethod.POST)
//	public ResultMap status(@PathVariable String appName, String instanceId, String status){
//		/*Application application = eurekaClient.getApplication(appName);
//		InstanceInfo instanceInfo = application.getByInstanceId(instanceId);
//		instanceInfo.setStatus(InstanceStatus.toEnum(status));*/
//		Application application = eurekaClient.getApplication(appName);
//		InstanceInfo instanceInfo = application.getByInstanceId(instanceId);
//		instanceInfo.setStatus(InstanceStatus.toEnum(status));
//		Map<String, String> headers = new HashMap<>();
//		headers.put("Content-Type", "text/plain");
////		HttpUtil.post(instanceInfo.getHomePageUrl() + "service-registry/instance-status", status, headers);
//		//http://localhost:1001/eureka/apps/UNCODE-ADMIN/DESKTOP-JL9T2IK:uncode-admin:8000/status?value=DOWN
//		return ResultMap.buildSuccess();
//	}
//	
//	
//	/**
//	 * @description 获取服务数量和节点数量
//	 */
//	@RequestMapping(value = "home", method = RequestMethod.GET)
//	public ResultMap home(){
//		List<Application> apps = eurekaClient.getApplications().getRegisteredApplications();
//		int appCount = apps.size();
//		int nodeCount = 0;
//		int enableNodeCount = 0;
//		for(Application app : apps){
//			nodeCount += app.getInstancesAsIsFromEureka().size();
//			List<InstanceInfo> instances = app.getInstances();
//			for(InstanceInfo instance : instances){
//				if(instance.getStatus().name().equals(InstanceStatus.UP.name())){
//					enableNodeCount ++;
//				}
//			}
//		}
//		return ResultMap.buildSuccess().put("appCount", appCount).put("nodeCount", nodeCount).put("enableNodeCount", enableNodeCount);
//	}
//	
//	/**
//	 * @description 获取所有服务节点
//	 */
//	@RequestMapping(value = "apps", method = RequestMethod.GET)
//	public ResultMap apps(){
//		List<Application> apps = eurekaClient.getApplications().getRegisteredApplications();
//		Collections.sort(apps, new Comparator<Application>() {
//	        public int compare(Application l, Application r) {
//	            return l.getName().compareTo(r.getName());
//	        }
//	    });
//		for(Application app : apps){
//			Collections.sort(app.getInstances(), new Comparator<InstanceInfo>() {
//		        public int compare(InstanceInfo l, InstanceInfo r) {
//		            return l.getPort() - r.getPort();
//		        }
//		    });
//		}
//		List<Map<String, Object>> list = new ArrayList<>();
//		for(Application app : apps){
//			List<InstanceInfo> instances = app.getInstancesAsIsFromEureka();
//			for(InstanceInfo instance : instances){
//				Map<String, Object> item = new HashMap<>();
//				item.put("name", app.getName());
//				item.put("status", instance.getStatus());
//				item.put("ipAddr", instance.getIPAddr());
//				item.put("instanceId", instance.getInstanceId());
////				item.put("registrationTimestamp", DateUtil.format(instance.getLeaseInfo().getRegistrationTimestamp()));
////				item.put("serviceUpTimestamp", DateUtil.format(instance.getLeaseInfo().getServiceUpTimestamp()));
////				item.put("lastUpdatedTimestamp", DateUtil.format(instance.getLastDirtyTimestamp()));
//				item.put("renewalIntervalInSecs", instance.getLeaseInfo().getRenewalIntervalInSecs());  	
//				item.put("info", instance.getStatusPageUrl());
//				item.put("version", instance.getMetadata().get("app-version")==null?"":instance.getMetadata().get("app-version"));
//				list.add(item);
//			}
//		}
//		return ResultMap.buildSuccess().put("list", list);
//	}

}
