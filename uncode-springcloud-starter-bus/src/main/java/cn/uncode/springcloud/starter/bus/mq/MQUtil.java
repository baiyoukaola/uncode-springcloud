package cn.uncode.springcloud.starter.bus.mq;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;

public final class MQUtil {
	
	private MQUtil() {}
	
	private static WrapMQTemplate MQ_TEMPLATE;
	
	public static void setMQTemplate(WrapMQTemplate mqTemplate) {
		MQ_TEMPLATE = mqTemplate;
	}
	
	public static WrapMQTemplate getTemplate() {
		return MQ_TEMPLATE;
	}
	
	public static void send(String exchangeName, String topic, Object object) {
		//to do

	}

}
