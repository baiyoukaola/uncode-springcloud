package cn.uncode.springcloud.gateway.canary;


import org.springframework.http.server.reactive.ServerHttpRequest;

import cn.uncode.springcloud.gateway.ribbon.RibbonContext;

public interface CanaryStrategy {
	
	String DEFAULT_STRATEGY_TYPE_IP = "ip";
	
	String DEFAULT_STRATEGY_TYPE_REQUEST = "keyInRequest";
	
	String DEFAULT_STRATEGY_TYPE_SESSION = "keyInsession";
	
	String FEIGN_CANARY_HEAD_KEY = "canary";
	
	
	RibbonContext resolve(ServerHttpRequest request);
	
	

}
